# [Alien4Cloud](https://alien4cloud.github.io/) plugin - [Clouni](https://bura2017.github.io/clouni.github.io/en/dirhtml/index.html) provider
A simple A4C plugin providing support of ISPRAS' TOSCA-orchestrator Clouni.
<hr>
This plugin:

* adds orchestrator Clouni to list of orchestrators.
* adds locations OpenStack, AWS, Kubernetes.

## Build
1. `mvn clean`
2. `mvn install`

## Set up
Yet no special configuration needed. Just upload `target/alien4cloud-clouni-provider-3.5.0-SNAPSHOT.zip` 
(`mvn install` output) to Alien4Cloud.

[comment]: <> (![Modifier Setup]&#40;doc/setupModifier.png?raw=true "Modifier setup"&#41;)

## Usage
1. Create and connect Clouni orchestrator.
2. Choose one of provided locations before deploy.
3. At deployment phase you will see your deploy playbook in deployment logs.
