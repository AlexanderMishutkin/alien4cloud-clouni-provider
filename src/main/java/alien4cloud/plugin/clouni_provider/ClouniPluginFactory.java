package alien4cloud.plugin.clouni_provider;

import alien4cloud.model.orchestrators.ArtifactSupport;
import alien4cloud.model.orchestrators.locations.LocationSupport;
import alien4cloud.orchestrators.plugin.IOrchestratorPluginFactory;
import com.google.common.collect.Maps;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.alien4cloud.tosca.model.definitions.PropertyDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ClouniPluginFactory implements IOrchestratorPluginFactory<ClouniOrchestrator, ProviderConfiguration> {
    @Inject
    protected ApplicationContext applicationContext;

    @SneakyThrows
    @Override
    public ClouniOrchestrator newInstance(ProviderConfiguration providerConfiguration) {
        log.info("ClouniPluginFactory.newInstance");

        RestTemplate restTemplate = new RestTemplate();
        String resourceUrl = providerConfiguration.getUrlYorc() + "/back/deploy?location=" +
                URLEncoder.encode("Testik", StandardCharsets.UTF_8.name()) +
                "&deployment=" + URLEncoder.encode("TTESTIK | 3214", StandardCharsets.UTF_8.name()) +
                "&paas=" + URLEncoder.encode("TTESTIK | 3214", StandardCharsets.UTF_8.name()) +
                "&environment=" + URLEncoder.encode("TTESTIK | 3214", StandardCharsets.UTF_8.name()) +
                "&version=" + URLEncoder.encode("TTESTIK | 3214", StandardCharsets.UTF_8.name()) +
                "&src=" + URLEncoder.encode("TTESTIK | 3214", StandardCharsets.UTF_8.name()) +
                "&source=" + URLEncoder.encode("TTESTIK | 3214", StandardCharsets.UTF_8.name());
        log.info(resourceUrl);
        ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
        log.info(response.getBody());

        return applicationContext.getBean(ClouniOrchestrator.class);
    }

    @Override
    public void destroy(ClouniOrchestrator clouniOrchestrator) {
        log.info("ClouniPluginFactory.destroy");
    }

    @Override
    public ProviderConfiguration getDefaultConfiguration() {
        log.info("ClouniPluginFactory.getDefaultConfiguration");
        return new ProviderConfiguration();
    }

    @Override
    public Class<ProviderConfiguration> getConfigurationType() {
        log.info("ClouniPluginFactory.getConfigurationType");
        return ProviderConfiguration.class;
    }

    @Override
    public LocationSupport getLocationSupport() {
        log.info("ClouniPluginFactory.getLocationSupport");
        return new LocationSupport(true , new String[]{
                "Ansible Playbook | OpenStack",
                "Ansible Playbook | AWS",
                "Ansible Playbook | Kubernetes",
        });
    }

    @Override
    public ArtifactSupport getArtifactSupport() {
        log.info("ClouniPluginFactory.getArtifactSupport");
        // Support no artifacts
        return new ArtifactSupport(new String[]{});
    }

    @Override
    public Map<String, PropertyDefinition> getDeploymentPropertyDefinitions() {
        log.info("ClouniPluginFactory.getDeploymentPropertyDefinitions");
        return new HashMap<String, PropertyDefinition>();
    }

    @Override
    public String getType() {
        log.info("ClouniPluginFactory.getType");
        return "Clouni Orchestrator";
    }

    @Override
    public void delete(String id) {
        log.info("ClouniPluginFactory.delete");
        IOrchestratorPluginFactory.super.delete(id);
    }
}
