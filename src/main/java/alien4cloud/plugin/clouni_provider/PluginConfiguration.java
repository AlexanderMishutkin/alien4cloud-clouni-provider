package alien4cloud.plugin.clouni_provider;

import alien4cloud.deployment.DeploymentTopologyDTOBuilder;
import alien4cloud.deployment.matching.services.nodes.DefaultNodeMatcher;
import alien4cloud.deployment.matching.services.nodes.NodeMatcherService;
import org.alien4cloud.alm.deployment.configuration.flow.FlowExecutor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.jmx.support.RegistrationPolicy;

import javax.inject.Inject;

@Order(-10)
@Configuration
@ComponentScan(basePackages = "alien4cloud.plugin.clouni_provider")
public class PluginConfiguration {
    @Bean("clouni-orchestrator-factory")
    public ClouniPluginFactory clouniOrchestratorFactory() {
        return new ClouniPluginFactory();
    }
}
