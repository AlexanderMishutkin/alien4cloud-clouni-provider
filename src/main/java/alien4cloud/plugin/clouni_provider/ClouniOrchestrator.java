package alien4cloud.plugin.clouni_provider;

import alien4cloud.model.deployment.DeploymentTopology;
import alien4cloud.model.runtime.Execution;
import alien4cloud.orchestrators.plugin.ILocationConfiguratorPlugin;
import alien4cloud.orchestrators.plugin.IOrchestratorPlugin;
import alien4cloud.orchestrators.plugin.model.PluginArchive;
import alien4cloud.paas.IPaaSCallback;
import alien4cloud.paas.exception.MaintenanceModeException;
import alien4cloud.paas.exception.OperationExecutionException;
import alien4cloud.paas.exception.PluginConfigurationException;
import alien4cloud.paas.model.*;
import alien4cloud.rest.deployment.DeploymentTopologyController;
import com.google.common.collect.Lists;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Slf4j
@Component
public class ClouniOrchestrator implements IOrchestratorPlugin<ProviderConfiguration> {
    private boolean flag = false;

    @Inject
    protected ApplicationContext applicationContext;

    @Inject
    private ClouniArchiveParser archiveService;

    @Inject
    private DeploymentTopologyController controller;
    private String s;
    private ProviderConfiguration providerConfiguration;

    @Override
    public ILocationConfiguratorPlugin getConfigurator(String locationType) {
        log.info("ClouniOrchestrator.getConfigurator for " + locationType);
        return applicationContext.getBean(ClouniLocationConfigurator.class);
    }

    @Override
    public List<PluginArchive> pluginArchives() {
        log.info("ClouniOrchestrator.pluginArchives");
        List<PluginArchive> archives = Lists.newArrayList();
        archives.add(archiveService.parsePluginArchives("commons/resources"));
        return archives;
    }

    /**
     * @param s
     * @param providerConfiguration
     * @deprecated
     */
    @Override
    public void setConfiguration(String s, ProviderConfiguration providerConfiguration) throws PluginConfigurationException {
        this.s = s;
        this.providerConfiguration = providerConfiguration;
        log.info("ClouniOrchestrator.setConfiguration:\n" + s);
    }

    @Override
    public Set<String> init(Map<String, String> map) {
        log.info("ClouniOrchestrator.init");
        return new HashSet<>();
    }

    @SneakyThrows
    @Override
    public void deploy(PaaSTopologyDeploymentContext context, IPaaSCallback<?> iPaaSCallback) {
        log.info(".deploy()");

        RestTemplate restTemplate = new RestTemplate();
        String resourceUrl = providerConfiguration.getUrlYorc() + "/back/deploy?location=" +
                URLEncoder.encode(s, StandardCharsets.UTF_8.name()) +
                "&deployment=" + URLEncoder.encode(context.getDeploymentId(), StandardCharsets.UTF_8.name()) +
                "&paas=" + URLEncoder.encode(context.getDeploymentPaaSId(), StandardCharsets.UTF_8.name()) +
                "&environment=" + URLEncoder.encode(context.getDeployment().getEnvironmentId(), StandardCharsets.UTF_8.name()) +
                "&version=" + URLEncoder.encode(context.getDeployment().getVersionId(), StandardCharsets.UTF_8.name()) +
                "&src=" + URLEncoder.encode(context.getDeployment().getSourceName(), StandardCharsets.UTF_8.name()) +
                "&source=" + URLEncoder.encode(context.getDeployment().getSourceId(), StandardCharsets.UTF_8.name());

        log.info(resourceUrl);
        ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
        log.info(response.getBody());

        iPaaSCallback.onFailure(new RuntimeException("Delegated to Clouni. Go to /front/deploy to see result..."));
    }

    @Override
    public void update(PaaSTopologyDeploymentContext paaSTopologyDeploymentContext, IPaaSCallback<?> iPaaSCallback) {
        log.info("ClouniOrchestrator.update");
        iPaaSCallback.onSuccess(null);
    }

    @Override
    public void undeploy(PaaSDeploymentContext paaSDeploymentContext, IPaaSCallback<?> iPaaSCallback, boolean b) {
        log.info("ClouniOrchestrator.undeploy");
        flag = false;
        iPaaSCallback.onSuccess(null);
    }

    @Override
    public void purge(PaaSDeploymentContext paaSDeploymentContext, IPaaSCallback<?> iPaaSCallback) {
        log.info("ClouniOrchestrator.purge");
        iPaaSCallback.onSuccess(null);
    }

    @Override
    public void resume(PaaSDeploymentContext paaSDeploymentContext, Execution execution, IPaaSCallback<?> iPaaSCallback) {
        log.info("ClouniOrchestrator.resume");
        iPaaSCallback.onSuccess(null);
    }

    @Override
    public void resetStep(PaaSDeploymentContext paaSDeploymentContext, Execution execution, String s, boolean b, IPaaSCallback<?> iPaaSCallback) {
        log.info("ClouniOrchestrator.resetStep");
        iPaaSCallback.onSuccess(null);
    }

    @Override
    public void scale(PaaSDeploymentContext paaSDeploymentContext, String s, int i, IPaaSCallback<?> iPaaSCallback) {
        log.info("ClouniOrchestrator.scale");
        iPaaSCallback.onSuccess(null);
    }

    @Override
    public void launchWorkflow(PaaSDeploymentContext paaSDeploymentContext, String s, Map<String, Object> map, IPaaSCallback<String> iPaaSCallback) {
        log.info("ClouniOrchestrator.launchWorkflow");
        iPaaSCallback.onSuccess(null);
    }

    @Override
    public void cancelTask(PaaSDeploymentContext paaSDeploymentContext, String s, IPaaSCallback<String> iPaaSCallback) {
        log.info("ClouniOrchestrator.cancelTask");
        iPaaSCallback.onSuccess(null);
    }

    @Override
    public void getStatus(PaaSDeploymentContext paaSDeploymentContext, IPaaSCallback<DeploymentStatus> iPaaSCallback) {
        log.info("ClouniOrchestrator.getStatus");
        if (flag) {
            iPaaSCallback.onSuccess(DeploymentStatus.DEPLOYED);
        } else {
            iPaaSCallback.onSuccess(DeploymentStatus.UNDEPLOYED);
        }
    }

    @Override
    public void getInstancesInformation(PaaSTopologyDeploymentContext paaSTopologyDeploymentContext, IPaaSCallback<Map<String, Map<String, InstanceInformation>>> iPaaSCallback) {
        log.info("ClouniOrchestrator.getInstancesInformation");

        iPaaSCallback.onSuccess(new HashMap<>());
    }

    @Override
    public void getEventsSince(Date date, int i, IPaaSCallback<AbstractMonitorEvent[]> iPaaSCallback) {
        log.info("ClouniOrchestrator.getEventsSince");
        iPaaSCallback.onSuccess(new AbstractMonitorEvent[]{});
    }

    @Override
    public void executeOperation(PaaSTopologyDeploymentContext paaSTopologyDeploymentContext, NodeOperationExecRequest nodeOperationExecRequest, IPaaSCallback<Map<String, String>> iPaaSCallback) throws OperationExecutionException {
        log.info("ClouniOrchestrator.executeOperation");
        iPaaSCallback.onSuccess(new HashMap<>());
    }

    @Override
    public void switchMaintenanceMode(PaaSDeploymentContext paaSDeploymentContext, boolean b) throws MaintenanceModeException {
        log.info("ClouniOrchestrator.switchMaintenanceMode");
    }

    @Override
    public void switchInstanceMaintenanceMode(PaaSDeploymentContext paaSDeploymentContext, String s, String s1, boolean b) throws MaintenanceModeException {
        log.info("ClouniOrchestrator.switchInstanceMaintenanceMode");
    }
}
